import { TTemplate } from '@/types/template';

export const defaultTemplateForm: TTemplate[] = [
  {
    name: 'name',
    element: 'input',
    display: 'Nom et prénom',
    value: '',
  },
  {
    name: 'age',
    element: 'input',
    value: '',
    display: 'âge',
  },
  {
    name: 'genre',
    element: 'select',
    values: ['Homme', 'Femme'],
    display: 'Genre',
  },
];
