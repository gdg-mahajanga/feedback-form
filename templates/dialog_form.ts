import { FormElements } from '@/types/element';
import { TTemplate } from '@/types/template';

export const dialogFormTemplate: TTemplate[] = [
  {
    name: 'name',
    element: 'input',
    display: 'Nom du champ (en minuscule)',
    value: '',
  },
  {
    name: 'display',
    element: 'input',
    value: '',
    display: 'Nom à afficher',
  },
  {
    name: 'element',
    element: 'select',
    values: FormElements,
    display: "Type d'élément",
  },
  {
    name: 'value',
    element: 'input',
    value: '',
    display: 'Valeurs',
  },
];
