import { defaultTemplateForm } from '@/templates/default_form';
import { dialogFormTemplate } from '@/templates/dialog_form';
import { TTemplate } from '@/types/template';
import { StateCreator } from 'zustand';

export type FormProps = {
  formElement: TTemplate[];
  addElement: (element: TTemplate) => void;
  dialogCreatorElement: TTemplate[];
  removeElement: (element: TTemplate) => void;
};

export const createFormSlice: StateCreator<FormProps> = (set) => ({
  formElement: defaultTemplateForm,
  dialogCreatorElement: dialogFormTemplate,
  removeElement: (element) =>
    set((state) => ({
      formElement: state.formElement.filter((e) => e.name !== element.name),
    })),
  addElement: (element) =>
    set((state) => ({
      formElement: [...state.formElement, element],
    })),
});
