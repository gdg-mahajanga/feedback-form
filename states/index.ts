import { create } from 'zustand';
import { FormProps, createFormSlice } from './form.state';
import { DialogProps, createDialogSlice } from './dialog.state';

export const useAppStore = create<FormProps & DialogProps>()((...a) => ({
  ...createFormSlice(...a),
  ...createDialogSlice(...a),
}));
