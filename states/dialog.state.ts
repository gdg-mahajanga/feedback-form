import { StateCreator } from 'zustand';

export type DialogProps = {
  dialogState: boolean;
  toggleDialog: () => void;
  closeDialog: () => void;
  openDialog: () => void;
};

export const createDialogSlice: StateCreator<DialogProps> = (set) => ({
  dialogState: false,
  toggleDialog: () =>
    set((state) => ({
      dialogState: !state.dialogState,
    })),
  closeDialog: () =>
    set((state) => ({
      dialogState: false,
    })),
  openDialog: () =>
    set((state) => ({
      dialogState: true,
    })),
});
