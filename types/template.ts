export type TSingleTemplate = {
  name: string;
  display: string;
  description?: string;
  element: 'input' | 'textarea';
  value: string;
};

export type TMultipleTemplate = {
  name: string;
  display: string;
  description?: string;
  element: 'select' | 'checkbox' | 'radio' | 'textarea';
  values: string[];
};

export type TTemplate = (TSingleTemplate | TMultipleTemplate) & {
  required?: boolean;
};
