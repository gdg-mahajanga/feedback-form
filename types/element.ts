export const UniqueValueElements = ['input', 'textarea'];
export const MultipleValuesElements = ['select', 'checkbox', 'radio'];

export const FormElements = [...UniqueValueElements, ...MultipleValuesElements];

export type TUniqueValueFormElement = (typeof UniqueValueElements)[number];
export type TMultipleValuesFormElement =
  (typeof MultipleValuesElements)[number];

export type TFormElement = TUniqueValueFormElement & TMultipleValuesFormElement;
