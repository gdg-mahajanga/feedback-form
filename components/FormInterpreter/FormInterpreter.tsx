'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { ControllerRenderProps, useForm } from 'react-hook-form';
import * as z from 'zod';

import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import useZodGenerator from '@/hooks/useZodGenerator';
import { Textarea } from '../ui/textarea';
import { TFormElement } from '@/types/element';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '../ui/select';
import { TTemplate } from '@/types/template';

export default function FormInterpreter({
  formElement,
  actionTitle = 'Sauvegarder',
  action,
}: {
  formElement: TTemplate[];
  actionTitle?: string;
  action?: (values: z.infer<typeof formSchema>) => void;
}) {
  const response = useZodGenerator(formElement);
  const formSchema = z.object(response);

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      event_name: '',
    },
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    if (action) {
      action(values);
    }
  }

  const elementMapper = (
    element: TTemplate,
    field: ControllerRenderProps<{}, never>
  ) => {
    switch (element.element) {
      case 'input':
        return <Input placeholder='' {...field} />;
      case 'select':
        return (
          <Select onValueChange={field.onChange} defaultValue={field.value}>
            <FormControl>
              <SelectTrigger>
                <SelectValue placeholder={element.display} />
              </SelectTrigger>
            </FormControl>
            <SelectContent>
              {element.values.map((item, index) => (
                <SelectItem value={item as string} key={index}>
                  {item}
                </SelectItem>
              ))}
            </SelectContent>
          </Select>
        );
      case 'radio':
        return <Select></Select>;
      case 'checkbox':
        return <Select></Select>;
      default:
        return <Textarea placeholder='' {...field} />;
    }
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)} className='space-y-4'>
        {formElement.map((item, index) => (
          <FormField
            key={`form_${index}`}
            control={form.control}
            name={item.name as never}
            render={({ field }) => (
              <FormItem>
                <FormLabel className='capitalize'>{item.display}</FormLabel>
                <FormControl>{elementMapper(item, field)}</FormControl>
                {item.description && (
                  <FormDescription>{item.description}</FormDescription>
                )}
                <FormMessage />
              </FormItem>
            )}
          />
        ))}
        <Button type='submit'>{actionTitle}</Button>
      </form>
    </Form>
  );
}
