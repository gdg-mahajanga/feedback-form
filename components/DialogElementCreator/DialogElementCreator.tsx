import { useAppStore } from '@/states';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from '../ui/dialog';
import { Button } from '../ui/button';
import FormInterpreter from '../FormInterpreter/FormInterpreter';
import {
  TMultipleTemplate,
  TSingleTemplate,
  TTemplate,
} from '@/types/template';
import { MultipleValuesElements } from '@/types/element';

export default function DialogElementCreator() {
  const { dialogState, closeDialog, dialogCreatorElement, addElement } =
    useAppStore();

  return (
    <Dialog open={dialogState}>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Are you absolutely sure?</DialogTitle>
          <DialogDescription>
            <FormInterpreter
              action={(values) => {
                let newValue: TTemplate = values as TSingleTemplate;
                if (
                  MultipleValuesElements.includes((values as TTemplate).element)
                ) {
                  
                }
                addElement(newValue as TTemplate);
                closeDialog();
              }}
              formElement={dialogCreatorElement}
            />
          </DialogDescription>
        </DialogHeader>
        <DialogFooter className='sm:justify-start'>
          <DialogClose asChild>
            <Button onClick={closeDialog} type='button' variant='secondary'>
              Fermer
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}
