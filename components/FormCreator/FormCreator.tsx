'use client';

import { useAppStore } from '@/states';
import FormInterpreter from '../FormInterpreter/FormInterpreter';
import { Button } from '../ui/button';
import DialogElementCreator from '../DialogElementCreator/DialogElementCreator';

export default function FormCreator() {
  console.log('Reload Formcreator');
  const { formElement, openDialog } = useAppStore();

  return (
    <section>
      <DialogElementCreator />
      <FormInterpreter formElement={formElement} />
      <Button onClick={openDialog}>Ajouter un élément</Button>
    </section>
  );
}
