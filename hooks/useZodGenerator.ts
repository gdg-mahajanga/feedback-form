import { TTemplate } from '@/types/template';
import * as z from 'zod';

export default function useZodGenerator(data: TTemplate[]) {
  const zodObject = data.reduce((p, c) => {
    return { [c.name]: z.string(), ...p };
  }, {});

  return zodObject;
}
