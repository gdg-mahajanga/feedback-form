import FormCreator from '@/components/FormCreator/FormCreator';

export default function CreatePage() {
  return (
    <main className='px-4 lg:px-64'>
      <FormCreator />
    </main>
  );
}
