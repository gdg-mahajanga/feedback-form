import Link from 'next/link';

export default function Home() {
  return (
    <main>
      <Link href='/create'>Créer un formulaire</Link>
    </main>
  );
}
